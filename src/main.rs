use async_kraken_ws::client::KrakenWS;

fn get_keys() -> (String, String) {
    let content = std::fs::read_to_string("key").expect("File not found");
    let lines: Vec<&str> = content.lines().collect();

    let key = String::from(lines[0]);
    let secret = String::from(lines[1]);

    (key, secret)
}

#[async_std::main]
async fn main() {
    let (api_key, api_secret) = get_keys();
    let kpr = KrakenWS::run_private(|x| println!("{}", x.to_string()), api_key, api_secret).await;
    let _ = kpr.subscribe_open_orders().await;
    let _ = kpr.subscribe_own_trades().await;

    let kpu = KrakenWS::run_public(|x| println!("{}", x.to_string())).await;
    let _ = kpu.subscribe_ticker(vec!["XBT/EUR"]).await;
    let _ = kpu.subscribe_ohlc(vec!["XBT/EUR"], 1).await;
    let _ = kpu.subscribe_book(vec!["XBT/EUR"], 100).await;
    let _ = kpu.subscribe_spread(vec!["XBT/EUR"]).await;
    let _ = kpu.subscribe_trade(vec!["XBT/EUR"]).await;

    loop {
        std::thread::sleep(std::time::Duration::from_secs(30));
        let _ = kpu.ping().await;
        let _ = kpr.ping().await;
    }
}
